FROM alpine:latest

ADD zig-linux-x86_64-0.10.0.tar.xz /opt/
RUN ln -s /opt/zig-linux-x86_64-0.10.0/zig /usr/local/bin/
